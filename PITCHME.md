## Comparison of modern platforms for massive data processing

<span style="color:gray; font-size:0.6em;">author: Wojciech Pituła </span>

<span style="color:gray; font-size:0.6em;">supervisor: PhD. Paweł Czarnul </span>

+++

![SLIDE](assets/Processing-kinds.png)

+++

![SLIDE](assets/Comparison-overview.png)

---

### Overview

+++ 

#### Problems

- Select tools 
- Select metrics
- Gather the data
- Compare

+++

#### Selected tools

- Apache Spark
- Apache Flink
- Hadoop MapReduce
- Apache Storm
- Apache Samza
- Twitter Heron
- Apache Gearpump
- Apache Apex
- Kafka Streams

+++

#### Status

- Done:
  - Selecting tools
  - Selecting metrics
  - Gathering data for most of the metrics

- To-Do:
  - Basic benchamrking
  - Summary

---

### Processing model

+++

#### Processing paradigm

- Batch processing
- Stream processing

+++

#### Complexity

- Simple
- Complex

+++

#### Generality

- Datasources
  - Multiple 
  - Single native
  - Single
- Execution environments
  - Multiple
  - Standalone-only
  - YARN-only
  - Any
  
+++

#### Message delivery semantics

- at-least-once
- at-most-once
- exactly-once


---

### Developer's perspective

+++

#### Programming API: Abstraction level

- Processor-based
- DSL-based

+++

#### Programming API: Langauge

- Scala
- Java
- Python
- R

+++

#### Programming API: Type-safety

- Strongly-typed
- Weakly-typed

+++

#### Programming API: External APIs

--- 

### Operation's perspective

- Deployment environment
  - Local
  - Standalone
  - YARN
  - Mesos
- Monitoring
- Security

---

### Manager's perspective

- Metrics
  - Popularity
  - Trends
  - Maturity
  - Data sources
    - Stackoverflow
    - Google trends
    - Github
- Licensing
- Enterprise support

---

### Thanks
